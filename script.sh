#! /bin/bash
# Prérequis:
#   - installation de pandoc
# commandes: 
#   - `bash script.sh` depuis le répertoire pour executer le script

source configuration.ini

## Contôle du type de mise à jour.
## Si les métadonnées ou le thème ont été mis à jour 
## - alors: on fait une génération complète du site pour prendre en compte les modifications
## - sinon: on fait une mise à jour partielle du site pour ne gérer que les contenus modifiés dans `contenus`
source application/proprietes.sh
if [[ $(date_modification "metadonnees") == $(date "+%F") || $(date_modification "theme") == $(date "+%F") ]];
then mise_a_jour_complete=true;
else mise_a_jour_complete=false;
fi

## Fonctions de metadonnées
source application/metadonnees.sh
## Fonctions de construction du site
source application/site.sh
construire

---
gabarit: home
robots: index,follow
title: Se créer un site sobre
description: Générateur de site statique 
---

## Origine du projet

Depuis mes débuts dans le domaine de l'écoconception numérique en 2019, ma vision de ce qu'est cette discipline à beaucoup évoluée. Aujourd'hui, ma prestation de conception web basée sur le SGC (Système de Gestion de Contenus) [Translucide (Nouvel Onglet)](https://www.translucide.net/){target="_blank"} ne me permet pas de répondre entièrement aux bonnes pratiques de l'écoconception. En effet, si la première de celles-ci est l'élimination des fonctionnalités non essentielles, alors son utilisation doit être également questionnée.

De cette réflexion, je propose, depuis 2022, pour de "simple" plaquettes numérique, le développement de sites statiques (sans interface de gestion de contenu) mono-page en vue d'éviter la mise en place d'un environnement technique complexe et plus couteux.

Néanmoins, il existe, entre ces deux solutions, un vide que j'aimerais combler: pouvoir doter celleux qui le souhaitent d'un site statique tout en lui permettant de garder la main sur la gestion de son contenu. 

Ce projet est donc le fruit de mes réflexions et de mes aspirations à développer un outil de gestion de contenu avec la couverture fonctionnelle et technique la plus sobre possible afin qu'il puisse est pris en main par des néophytes.

*Mathieu Vigou Didierjean*

## À propos

Actuellement, ce projet est en version alpha et est expérimenté pour la gestion du contenu du site [librae.fr](https://librae.fr){target="_blank"}
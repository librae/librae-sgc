#! /bin/bash

# Ce script contient l'ensemble des fonctions permettant de gérer les fichiers de métadonnées
# du système de gestion de contenu
# cf. `contenus/_metadonnes/*.md`

# Traduit un fichier du dossier metadonnées passé en paramètre
# Paramètres:
#   - $1 = nom du fichier Markdown
function metadonnees {
   # attention: ne pas supprimer le `_` avant $FUNCNAME
   fichier="$dossier_contenus/_$FUNCNAME/$1.md"
   if [[ -f $fichier ]];
   then echo $( pandoc -t markdow -t HTML5 $fichier )
   fi
}

## Traduit le fichier de `metadonnees/header.md` en html
function header {
   echo $(metadonnees $FUNCNAME);
}

## Traduit le fichier de `metadonnees/navigation.md` en html
function navigation {
   echo $(metadonnees $FUNCNAME);
}

## Traduit le fichier de `metadonnees/footer.md` en html
function footer {
   echo $(metadonnees $FUNCNAME);
}


#! /bin/bash


# Ce script contient l'ensemble des fonctions permettant de générer le site web

theme=$( if [[ -n $theme ]]; then echo "theme/$theme"; else echo "theme"; fi );

# Description:
#   Fonction principale de l'application permettant le génération du site web à partir des fichiers markdown
# Paramètres:
#   - $1 = nom du dossier contenant les fichier Markdown
function construire {

   # Début des traitements
   dossierLu="${1:-$dossier_contenus}"
   dossierCible=${dossierLu/$(echo $dossierLu/ | cut -d '/' -f 1)/$dossier_site}
   
   # docs: création du dossier cible s'il n'existe pas
   if [[ ! -d "$dossierCible" ]]; then mkdir "$dossierCible"; fi

   # docs: lecture récursive du dossier de contenu
   for element in $dossierLu/*
   do

      source__chemin="$element"
      source__dirname=$(dirname "$source__chemin")
      source__nom=$(basename "$source__chemin")
      cible__nom="$( basename "$( echo $( url "$source__nom" ) )" .md ).html"
      cible__chemin="$dossierCible/$cible__nom"


      # docs: Les éléments préfixés par un `_` sont ignorés et utilisés comme brouillon
      if [[ $( [[ $(echo "$(basename $element)" | grep -E "^_") ]] && echo true ) ]];
      then continue;
      else

         profondeur=$(echo $source__chemin | grep -o "/" | wc -l)
         titre=$( titre )
         echo $dossier_site;
         css="$( echo $( dirname $cible__chemin) | sed "s|^$dossier_site|..|g" | sed -r "s/(\w+)/../g" )/style.min.css"
         echo $css;

         # docs: traitement des dossiers
         if [[ -d "$element" ]]; then

            # docs: initialisation d'un fichier temporaire index permettant de lister le contenu du dossier
            echo > "$element/.index"
            # docs: appel récursif de la fonction afin de générer les pages du site à partir des fichiers markdown du dossier
            echo "$(construire "$element")"
            # docs: création de l'index.html du dossier lu

            # appel du fichier de configuration du dossier lu
            if [[ -f $element/.ini ]]; then 
               source $element/.ini
               if [[ $tri=="desc" ]]; then sort "$element/.index" -o "$element/.index"; fi
            fi
            page "$element/index.md" "$element/.index"
            rm "$element/.index"

            ######## echo "$(date), dossier, $(basename $element), statut: à traiter";

         fi

         # docs: traitement des fichiers Markdown
         if [[ -f $element ]];
         then 

            # On récupère les propriétés du fichier lu
            date_creation=$(date_creation "$source__chemin")
            date_modification=$(date_modification "$source__chemin")

            # On controle de nom du fichier pour extraire les informations éventuelle
            # nomenclature : AAAAMMJJ_Titre
            if [[ $( [[ $(echo "$titre" | grep -E "^[0-9]{4}[0-9]{2}[0-9]{2}_") ]] && echo true ) ]];
            then                
               date_publication=$( date -d $( echo $titre | cut -d "_" -f 1 ) +'%Y-%m-%d' );
               titre=$( echo $titre | cut -d "_" -f 2 )    
               cible__nom="$( basename "$( echo $( url "$titre" ) )" .md ).html"
               cible__chemin="$dossierCible/$cible__nom"
            fi;

            # docs: on ne met à jour les contenus que si la date de modification est supérieure ou égale à la date du jour ou qu'on doit regénerer tout le site (exemple: mise à jour des métadonnées)
            if [[ $date_modification < $(date "+%F") && ! $mise_a_jour_complete ]]; 
            then continue ######## echo "$(date), fichier, $source__nom, statut: pas de modifications"
            else
            
               dossierParcouru=$(echo $dossierLu/ | cut -d '/' -f 2)

               # docs: les fichiers `index` sont exclus de cette partie pour d'être générés en fin de traitement. Seul l'index principal, servant de page d'accueil est traité
               if [[ $source__nom != "index.md" || $( [[ $source__nom == "index.md" && $profondeur == 1 ]] && echo true) ]]; then 

                  # docs: indexation du fichier traité dans le fichier temporaire
                  indexer $cible__nom

                  # docs: création d'un fichier temporaire encodé sans espace pour éviter les erreurs de pandoc
                  fichierTemporaire__nom=$(url "$source__nom")
                  cp "$source__chemin" "$source__dirname/.$fichierTemporaire__nom"

                  page "$source__dirname/.$fichierTemporaire__nom"

                  # docs: suppression des fichiers temporaires
                  rm "$source__dirname/.$fichierTemporaire__nom"

               fi         
                  
               ######## echo "$(date), fichier, $source__nom, statut: à publier";
            fi
         fi

      fi
      
   done
}

# Description:
#   Transforme une chaine de caractère pour en faire un lien exloitable par le navigateur
# Paramètres:
#   - $1 = chaine de caractères
function url {
   echo $1 | tr '[:upper:]' '[:lower:]' | sed 's/áàâäçéèêëîïìôöóùúüñÂÀÄÇÉÈÊËÎÏÔÖÙÜÑ/aaaaceeeeiiiooouuunAAACEEEEIIOOUUN/g' | sed 's/ /-/g'
}

# Description:
#   Recherche le gabarit adapté au contenu à générer
# Paramètres:
#   - aucun
function gabarit {

   if [[ $( [[ $source__nom == "index.md" && $profondeur == 1 ]] && echo true) ]];
   then echo "accueil"
   elif [[ -f "$theme/gabarits/$( basename $element ).html" ]]
   then echo $( basename $element )
   else echo "page"
   fi

}

# Description:
#   Génère le `<titre>` de la page à partir du nom du fichier
# Paramètres:
#   - aucun
function titre {

   if [[ ! -z "$dossierParcouru" && $source__nom == "index.md" ]];
   then echo ${dossierParcouru^};
   elif [[ -z "$dossierParcouru" && $source__nom == "index.md" ]]
   then echo "index"
   else echo $( basename "${source__nom^}" .md );
   fi

}

# Description:
#   À partir d'un fichier lu, la fonction génère le fil d'ariane accessible au format HTML
# Paramètres:
#   - $1 = le chemin du fichier lu
function ariane {

   # Configuration de l'algorithme
   href=""

   racine="${cible__chemin/$(echo $dossier_site__chemin | cut -d '/' -f 1 )/""}/"

   compteur=2
   niveauArborescence=$(echo $racine | grep -o "/" | wc -l)

   breadcrumb="<ol typeof=\"BreadcrumbList\" vocab=\"https://schema.org/\">"
   breadcrumb=$breadcrumb"<li property=\"itemListElement\" typeof=\"ListItem\"><a href=\"/\">$nomDuSite</a></span></li>"

   # Tant que i est inférieur au nombre de sous dossier
   while [[ $compteur -lt $niveauArborescence ]]
   do
      span=$(echo $racine/ | cut -d '/' -f $compteur )
      # Si le compteur est supérieur à 1 et que le fichier généré n'est pas un index
      if [[ $compteur > 1 ]] && [[ "$cible__nom" != "index.html" ]]; 
      #alors, on ajoute un niveau dans le fil d'Ariane
      then
         href="$href/$span"
         breadcrumb=$breadcrumb"<li property=\"itemListElement\" typeof=\"ListItem\"><a href=\"$href/\">${span^}</a></li>"
      fi
      ((compteur++))
   done
   breadcrumb=$breadcrumb"<li property=\"itemListElement\" typeof=\"ListItem\"><span property=\"name\" aria-current=\"page\">$titre</span></li>"
   breadcrumb=$breadcrumb"</ol>"

   echo $breadcrumb

}

# Description:
#   Permet d'indexer, dans un fichier temporaire un fichier afin de constituer les pages de navigation intermédiaires
# Paramètres:
#   - $1 = chemin du fichier Markdown à transformer
#   - $2 = chemin du fichier Markdown à transformer (utilisé pour la génération de l'index des sous dossier)
function indexer {

   echo "- [$titre]($1) <time datetime=\"$date_publication\">$(date_FR $date_publication)</time>" >> "$source__dirname/.index"

}

# Description:
#   Génère le `<main>` du document HTML
# Paramètres:
#   - $1 = chemin du fichier Markdown à transformer
#   - $2 = chemin du fichier Markdown à transformer (utilisé pour la génération de l'index des sous dossier)
function contenu {

   pandoc \
   -t markdow -t HTML5 \
   -V title="$titre" --metadata title="$titre" \
   --toc -V toc-titre="Dans cet article" \
   --toc-depth=2 \
   --template $theme/gabarits/$( gabarit ).html \
   $1 $2

}

# Description:
#   Génère la page complète au format HTML
# Paramètres:
#   - $1 = chemin du fichier Markdown à transformer
#   - $2 = chemin d'un second fichier Markdown à transformer (utilisé pour la génération de l'index des sous-dossiers)
function page {


   # aiguillage du fichier de sortie
   if [[ $( basename $1 ) == "index.md" ]]; 
   then page="${element/$(echo $element/ | cut -d '/' -f 1)/$dossier_site}/index.html"; 
   else page=$cible__chemin
   fi

   if [[ "$( gabarit )" != "accueil" ]]; then ariane=$(ariane); else ariane=""; fi

   pandoc \
      -f markdown -t HTML5 \
      -V title="$titre" --metadata title="$titre" \
      -V nomDuSite="$nomDuSite" \
      -s -c $css \
      -V header="$( header )" \
      -V navigation="$( navigation )" \
      --template $theme/archetype.html \
      $1 \
      -V ariane="$ariane" \
      -V contenu="$( contenu $1 $2 )" \
      -V footer="$( footer )" \
      -o $page \

}
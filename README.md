Depuis mes débuts dans le domaine de l'écoconception numérique en 2019, ma vision de ce qu'est cette discipline à beaucoup évoluée. Aujourd'hui, la prestation de conception web basée sur le SGC [Translucide](https://www.translucide.net/) ne permet pas de répondre aux bonnes pratiques de l'écoconception. En effet, si la première des bonnes pratiques est l'élimination des fonctionnalités non essentielles, alors la mise en place d'un SGC devrait être également questionnée. Translucide s'avère être un outil très accessible pour sa gestion de contenu mais ne s'avère, à mon sens, pas adapté à l'ensemble des projets pour lesquels on m'accorde sa confiance.

De cette réflexion, je propose, depuis 2022 le développement de site mono-page en vue d'éviter la mise en place de serveur pour une "simple" plaquette numérique. Cette solution permet également de réduire les impacts économiques liés à la mise en place et à la maintenance de tout cet environnement numérique.

Néanmoins, il existe, entre ces deux solutions, un vide que j'aimerais combler: pouvoir doter ma clientèle d'un site statique tout en lui permettant de garder la main sur la gestion de son contenu. 

Ce projet est donc le fruit de ces réflexions et de mes aspirations à développer un outil de gestion de contenu avec la couverture fonctionnelle et technique la plus sobre possible afin qu'il puisse est pris en main par des néophytes.

*Mathieu Vigou Didierjean*

# À propos

Actuellement, ce projet est en version *alpha* et est expérimenté pour la gestion du contenu du site [librae.fr](https://librae.fr)